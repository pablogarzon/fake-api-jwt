const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')
const config = require('./config')

const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()

function createToken(payload){
    return jwt.sign(payload, config.secret, {expiresIn: config.expiresIn})
}

function verifyToken(token){
    return jwt.verify(token, config.secret, (err, dec) => err == null)
}

server.use(middlewares)
server.use(jsonServer.bodyParser)
server.use(/^(?!.*\/login).*$/, (req, res, next) => {
    const auth = req.headers.authorization
    if(!!auth && auth.split(' ')[0] === 'Bearer' && verifyToken(auth.split(' ')[1]))
	setTimeout(() => next(), 5000)
    else
	res.status(401).json({'message': 'Your are not authorized'}) 
})

server.post('/login', (req, res, next) => {
    const payload = req.body
    if(payload.user === 'test' && payload.password === 'test')
	res.json({token: createToken(payload)})
    else
	res.status(401).json({err: 'invalid user or password'})
})

server.use(router)

server.listen(config.port, () => console.log('JSON-Server running in port ' + config.port))

